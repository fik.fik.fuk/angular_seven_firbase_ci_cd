import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  wikiList: AngularFireList<any>;
  wikis: any = [];
  loading: boolean = false;
  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    public authService: AuthService
  ) {
    this.wikiList = db.list('users');
  }

  ngOnInit(): void {

    this.loading = true;
    this.wikiList.snapshotChanges().subscribe(
      (res: any) => {
        this.wikis = res;
        this.loading = false;
        console.log(this.wikis)
      },
      (error: any) => {
        this.loading = false;
        alert(error);
      }
    );
  }

  delete(id: any) {
    this.db.list("/users").remove(id);
    // this.router.navigate(['/']);
    // alert(id)
  }

  test() {
    console.log(JSON.parse(localStorage.getItem('user') || '{}')
    )

  }

}
