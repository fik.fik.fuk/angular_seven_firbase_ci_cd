import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  wikiList: AngularFireList<any>;
  wikis: any = [];
  loading: boolean = false;
  prayer: any = { 'timings': { 'Fajr': '00:00', 'Dhuhr': '00:00', 'Asr': '00:00', 'Maghrib': '00:00', 'Isha': '00:00' } };
  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    private http: HttpClient,

  ) {
    this.wikiList = db.list('users');
  }

  ngOnInit(): void {

    // this.loading = true;
    // this.wikiList.snapshotChanges().subscribe(
    //   (res: any) => {
    //     this.wikis = res;
    //     this.loading = false;
    //     console.log(this.wikis)
    //   },
    //   (error: any) => {
    //     this.loading = false;
    //     alert(error);
    //   }
    // );

    this.getLocation()
  }

  delete(id: any) {
    this.db.list("/users").remove(id);
    // this.router.navigate(['/']);
    // alert(id)
  }

  test() {
    console.log(JSON.parse(localStorage.getItem('user') || '{}')
    )

  }

  getLocation(): void {
    this.loading = true;
    console.log('this.loading')
    console.log(localStorage.getItem("prayer"))

    if (localStorage.getItem("prayer") === null) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          const longitude = position.coords.longitude;
          const latitude = position.coords.latitude;
          this.callApi(longitude, latitude);
        });
      } else {
        console.log("No support for geolocation")
      }
      console.log('if')
    }
    else {
      console.log('already has prayer')
      this.prayer = JSON.parse(localStorage.getItem("prayer") || '{}');
      this.loading = false;
    }
  }

  callApi(Longitude: number, Latitude: number) {

    this.http.get(`http://api.aladhan.com/v1/timings/${Math.floor(Date.now() / 1000)}?latitude=${Latitude}&longitude=${Longitude}&method=1`).subscribe(
      (res: any) => {
        // console.log(res)
        localStorage.setItem("prayer", JSON.stringify(res.data));
        this.prayer = JSON.parse(localStorage.getItem("prayer") || '{}');
        this.loading = false;
      },
      (error: any) => {
        alert(error)
        this.loading = false;

      }
    );
  }

  Refresh() {
    localStorage.removeItem('prayer');
    this.getLocation()
  }
}
