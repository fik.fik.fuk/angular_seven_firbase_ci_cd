import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { NgForm } from '@angular/forms';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  constructor(
    private db: AngularFireDatabase,
    public router: Router
  ) { }

  ngOnInit(): void {
  }


  addChat(data: NgForm) {
    this.db.list("/chat").push(data.value);
    this.router.navigate(['/home'])

  }
}
