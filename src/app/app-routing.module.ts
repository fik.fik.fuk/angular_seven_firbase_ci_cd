import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContitionComponent } from './contition/contition.component';
import { PolicyComponent } from './policy/policy.component';
import { AuthGuard } from './shared/guard/auth.guard'

const routes: Routes = [
  {
    path: 'condition', component: ContitionComponent
  },
  {
    path: 'policy', component: PolicyComponent
  },
  {
    path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule), canActivate: [AuthGuard]
  },
  {
    path: '**', redirectTo: 'home', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
