// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    // apiKey: "AIzaSyAeRdeQjToSrafOYx-xrIrpGfRieQdppFM",
    // authDomain: "chat-together-6f432.firebaseapp.com",
    // databaseURL: "https://chat-together-6f432.firebaseio.com",
    // projectId: "chat-together-6f432",
    // storageBucket: "chat-together-6f432.appspot.com",
    // messagingSenderId: "408902537571",
    // appId: "1:408902537571:web:98351fdc8df731134d4356",
    // measurementId: "G-VNH0HWJYZ6",


    apiKey: "AIzaSyBf6AoO0A2mFYIbT5wMiQR8kexxrXZe4jQ",
    authDomain: "port-folio-d2979.firebaseapp.com",
    projectId: "port-folio-d2979",
    storageBucket: "port-folio-d2979.appspot.com",
    messagingSenderId: "444864023240",
    appId: "1:444864023240:web:4efa3905c88b388cc5b3fd",
    measurementId: "G-2VSDCCDQCT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
