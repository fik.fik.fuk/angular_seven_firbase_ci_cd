import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContitionComponent } from './contition.component';

describe('ContitionComponent', () => {
  let component: ContitionComponent;
  let fixture: ComponentFixture<ContitionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContitionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
