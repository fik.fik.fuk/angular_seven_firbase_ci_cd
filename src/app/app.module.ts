import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MdbModule } from 'mdb-angular-ui-kit';

import { firebaseConfig } from '../environments/firebase.config';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { APP_BASE_HREF } from '@angular/common';
import { PolicyComponent } from './policy/policy.component';
import { ContitionComponent } from './contition/contition.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { BackButtonDisableModule } from 'angular-disable-browser-back-button';

@NgModule({
  declarations: [
    AppComponent,
    PolicyComponent,
    ContitionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MdbModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    // BackButtonDisableModule.forRoot()

  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/angular_seven_firbase_ci_cd/' },],
  bootstrap: [AppComponent]
})
export class AppModule { }
