import { Component, OnInit, NgZone } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { AuthService } from "../../shared/services/auth.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  value = 'Clear me';

  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    public authService: AuthService,
    public ngZone: NgZone
  ) { }

  ngOnInit(): void {
  }



}
