import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from "../shared/guard/auth.guard";
import { SigninComponent } from './signin/signin.component';
import { ChatComponent } from './chat/chat.component';
// import { BackGuard } from "../shared/guard/back.guard";
import { ChatAllComponent } from './chat-all/chat-all.component';

const routes: Routes = [
  // {
  //   path: 'home', component: HomeComponent
  // },
  // {
  //   path: '', redirectTo: 'home', pathMatch: 'full'

  // }


  {
    path: '', component: UserComponent, children: [
      {
        path: 'profile', component: ProfileComponent
      },
      {
        path: 'chatAll', component: ChatAllComponent
      },
      {
        path: 'chat', component: ChatComponent
      },
      {
        path: '**', redirectTo: 'home', pathMatch: 'full'
      }

    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
