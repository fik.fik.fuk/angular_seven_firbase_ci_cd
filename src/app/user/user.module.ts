import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { HomeComponent } from './home/home.component';
import { SharedModule } from './../shared/shared.module';
import { UserComponent } from './user.component';
import { ProfileComponent } from './profile/profile.component';
import { SigninComponent } from './signin/signin.component';


@NgModule({
  declarations: [
    HomeComponent,
    UserComponent,
    ProfileComponent,
    SigninComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,

  ]
})
export class UserModule { }
