import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home/home.component'
import { HomeRoutingModule } from './home-routing.module'
import { SigninComponent } from './signin/signin.component';



@NgModule({
  declarations: [
    HomeComponent,
    SigninComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule

  ]
})
export class HomeModule { }
