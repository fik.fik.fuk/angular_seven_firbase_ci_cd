import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular';
  constructor() {
    const png = btoa(window.location.origin) + '.png';
    const href = window.location.origin + '/icons/' + png;

    const linkFav = document.createElement('link');
    linkFav.rel = 'icon';
    linkFav.href = href;
    document.head.appendChild(linkFav);

    const linkApp1 = document.createElement('link');
    const linkApp2 = document.createElement('link');
    const linkApp3 = document.createElement('link');
    const linkApp4 = document.createElement('link');
    linkApp1.rel = 'apple-touch-icon';
    linkApp2.rel = 'apple-touch-icon';
    linkApp3.rel = 'apple-touch-icon';
    linkApp4.rel = 'apple-touch-icon';
    linkApp2.setAttribute('sizes', '152x152');
    linkApp3.setAttribute('sizes', '167x167');
    linkApp4.setAttribute('sizes', '180x180');
    linkApp1.href = href;
    linkApp2.href = href;
    linkApp3.href = href;
    linkApp4.href = href;
    document.head.appendChild(linkApp1);
    document.head.appendChild(linkApp2);
    document.head.appendChild(linkApp3);
    document.head.appendChild(linkApp4);
  }
}
